package com.draco.malekojast.Utils;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.draco.malekojast.R;

public class Helping {

    // frame changer helping


    public static void frameAdd(FragmentManager fragmentManager, Fragment fragment) {
        fragmentManager.beginTransaction().add(R.id.frame_container, fragment).commit();
    }

    public static void frameCommint(FragmentManager fragmentManager, Fragment fragment) {
        fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
    }

    public static void frameCommintBackStack(FragmentManager fragmentManager, Fragment fragment) {
        fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).addToBackStack(null).commit();
    }


}

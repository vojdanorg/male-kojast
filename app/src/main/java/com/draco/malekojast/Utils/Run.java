package com.draco.malekojast.Utils;

import android.app.Application;

import com.draco.malekojast.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class Run extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        calligraphy();
    }

    private void calligraphy() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/yekan.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}

package com.draco.malekojast.UI.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.draco.malekojast.Models.CountryModel;
import com.draco.malekojast.R;
import com.draco.malekojast.UI.Activity.MainActivity;
import com.draco.malekojast.UI.Activity.MapActivity;
import com.draco.malekojast.Utils.Helping;
import com.draco.malekojast.ViewModel.SearchSiteViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {

    //    @BindView(R.id.txt_help)
//    TextView txtHelp;
    @BindView(R.id.edt_query)
    EditText edtQuery;
    @BindView(R.id.counter_iran_flaq)
    ImageView counterIranFlaq;
    @BindView(R.id.country_name)
    TextView countryName;
    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.info_layout)
    RelativeLayout infoLayout;
    @BindView(R.id.btn_search_query)
    Button btnSearchQuery;
    @BindView(R.id.half_price)
    TextView halfPrice;
    @BindView(R.id.showInMap)
    ImageButton showInMap;

    public static DashboardFragment self;
    double lat;
    double lng;
    @BindView(R.id.txt_coding_with)
    TextView txtCodingWith;
    @BindView(R.id.ac_cafe)
    ImageView acCafe;
    @BindView(R.id.txt_little_love)
    TextView txtLittleLove;
    @BindView(R.id.call_phone)
    ImageButton callPhone;

    private SearchSiteViewModel searchSiteViewModel;

    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        self = this;
        searchSiteViewModel = ViewModelProviders.of(this).get(SearchSiteViewModel.class);
        show_btn_search();
    }

    @OnClick(R.id.call_phone)
    void getSupport(){
        Helping.frameCommintBackStack(getFragmentManager(), new SupportFragment());
    }
    private void show_btn_search() {
        edtQuery.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 5) {
                    btnSearchQuery.setVisibility(View.GONE);
                    infoLayout.setVisibility(View.GONE);
                    btnSearchQuery.setBackground(getContext().getResources().getDrawable(R.drawable.btn_query_shape));
                    btnSearchQuery.setText("ماله کجاست؟");
                } else if (s.length() > 5) {
                    btnSearchQuery.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @OnClick(R.id.btn_search_query)
    void searchQuery() {
        String url = edtQuery.getText().toString().replace("http://", "").replace("https://", "");
        searchSiteViewModel.searchSites(url).observe(this, countryModel -> {
            getCounterInfo(countryModel);
            lat = countryModel.getLat();
            lng = countryModel.getLon();
        });
    }

    private void getCounterInfo(CountryModel countryModel) {
        infoLayout.setVisibility(View.VISIBLE);
        countryName.setText(countryModel.getCountry());
        location.setText(countryModel.getCity());
        if (countryModel.getCountryCode().equals("IR")) {
            counterIranFlaq.setVisibility(View.VISIBLE);
            halfPrice.setText("محتوای این سایت بصورت نیم بها محاسبه میگردد \n با خیال راحت استفاده کنید.");
            infoLayout.setBackground(getContext().getResources().getDrawable(R.drawable.bg_layout_info_success_iran));
            btnSearchQuery.setBackground(getContext().getResources().getDrawable(R.drawable.btn_query_shape_success));
            btnSearchQuery.setText("ایول =)");
        } else {
            counterIranFlaq.setVisibility(View.GONE);
            halfPrice.setText("محتوای این سایت بصورت نیم بها محاسبه نمیگردد \n در مصرف اینترنت کوشا باشید.");
            infoLayout.setBackground(getContext().getResources().getDrawable(R.drawable.bg_layout_info_other_counter));
            btnSearchQuery.setBackground(getContext().getResources().getDrawable(R.drawable.btn_query_shape));
            btnSearchQuery.setText("باشه :(");
        }

    }

    public void failRequest(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.showInMap)
    void showInMap() {
        Intent showInMap = new Intent(getContext(), MapActivity.class);
        showInMap.putExtra("MAP_LAT", lat);
        showInMap.putExtra("MAP_LNG", lng);
        startActivity(showInMap);
    }
}

package com.draco.malekojast.res;

import com.draco.malekojast.Models.CountryModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Api {
    @GET("/json/{site_url}")
    Call<CountryModel> searchSite(@Path("site_url") String siteUrl);
}

package com.draco.malekojast.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.draco.malekojast.Models.CountryModel;
import com.draco.malekojast.Repository.SearchSiteRepository;

public class SearchSiteViewModel extends AndroidViewModel {

    private LiveData<CountryModel> liveData;

    private SearchSiteRepository searchSiteViewModel;


    public SearchSiteViewModel(@NonNull Application application) {
        super(application);
        searchSiteViewModel = new SearchSiteRepository();
    }


    public LiveData<CountryModel> searchSites(String website_url){
        return liveData = searchSiteViewModel.searchSites(website_url);
    }
}
